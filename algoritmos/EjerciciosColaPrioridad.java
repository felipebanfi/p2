package algoritmos;

import implementaciones.DiccionarioMultipleDinamico;
import tdas.ColaPrioridadTDA;
import tdas.DiccionarioMultipleTDA;

public class EjerciciosColaPrioridad {

	public static boolean sonIdenticas(ColaPrioridadTDA cola1, ColaPrioridadTDA cola2) {
		boolean sonIdenticas = true;
		
		while (sonIdenticas && !cola1.estaVacia() && !cola2.estaVacia())
		{
			if (cola1.primero() != cola2.primero() ||
				cola1.prioridad() != cola2.prioridad())
			{
				sonIdenticas = false;
			}
			
			cola1.desacolar();
			cola2.desacolar();
		}
		
		if (!cola1.estaVacia() || !cola2.estaVacia())
		{
			sonIdenticas = false;
		}
		
		return sonIdenticas;
	}

	public static DiccionarioMultipleTDA valorAsociadoAPrioridades(ColaPrioridadTDA cola)
	{
		DiccionarioMultipleTDA d = new DiccionarioMultipleDinamico();
		d.inicializar();
		
		while (!cola.estaVacia())
		{
			d.agregar(cola.primero(), cola.prioridad());
			cola.desacolar();
		}
		
		return d;
	}

}
