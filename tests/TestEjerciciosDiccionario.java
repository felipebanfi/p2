package tests;

import static org.junit.Assert.*;
import implementaciones.DiccionarioMultipleDinamico;

import org.junit.Test;

import algoritmos.EjerciciosDiccionario;

import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class TestEjerciciosDiccionario
{

	@Test
	public void testTodasLasClavesTodosLosElementos()
	{
		DiccionarioMultipleTDA d1 = new DiccionarioMultipleDinamico();
		DiccionarioMultipleTDA d2 = new DiccionarioMultipleDinamico();
		
		d1.inicializar();
		
		d1.agregar(3,31);
		d1.agregar(3,37);
		d1.agregar(3,34);
		
		d1.agregar(7,71);
		
		d1.agregar(5,53);
		d1.agregar(5,58);
		
		d2.inicializar();
		
		d2.agregar(9,95);
		
		d2.agregar(7,73);
		d2.agregar(7,71);
		d2.agregar(7,75);
		
		d2.agregar(6,64);
		d2.agregar(6,67);
		
		d2.agregar(2,21);
		
		DiccionarioMultipleTDA d = EjerciciosDiccionario.todasLasClavesTodosLosElementos(d1, d2);
		ConjuntoTDA claves = d.claves();
		
		assertTrue(claves.pertenece(3));
		assertTrue(claves.pertenece(7));
		assertTrue(claves.pertenece(5));
		assertTrue(claves.pertenece(9));
		assertTrue(claves.pertenece(6));
		assertTrue(claves.pertenece(2));
		assertFalse(claves.pertenece(67));
		
		ConjuntoTDA valoresClave7 = d.recuperar(7);
		assertTrue(valoresClave7.pertenece(71));
		assertTrue(valoresClave7.pertenece(73));
		assertTrue(valoresClave7.pertenece(75));
		assertFalse(valoresClave7.pertenece(77));
		
		ConjuntoTDA valoresClave3 = d.recuperar(3);
		assertTrue(valoresClave3.pertenece(31));
		assertTrue(valoresClave3.pertenece(34));
		assertTrue(valoresClave3.pertenece(37));
	}

	@Test
	public void testSoloClavesCoincidentesTodosLosElementos()
	{
		DiccionarioMultipleTDA d1 = new DiccionarioMultipleDinamico();
		DiccionarioMultipleTDA d2 = new DiccionarioMultipleDinamico();
		
		d1.inicializar();
		
		d1.agregar(3,31);
		d1.agregar(3,37);
		d1.agregar(3,34);
		
		d1.agregar(7,71);
		
		d1.agregar(5,53);
		d1.agregar(5,58);
		
		d1.agregar(2,25);
		
		d2.inicializar();
		
		d2.agregar(9,95);
		
		d2.agregar(7,73);
		d2.agregar(7,71);
		d2.agregar(7,75);
		
		d2.agregar(6,64);
		d2.agregar(6,67);
		
		d2.agregar(2,21);
		
		DiccionarioMultipleTDA d = EjerciciosDiccionario.soloClavesCoincidentesTodosLosElementos(d1, d2);
		ConjuntoTDA claves = d.claves();
		
		assertTrue(claves.pertenece(7));
		assertTrue(claves.pertenece(2));
		assertFalse(claves.pertenece(3));
		assertFalse(claves.pertenece(5));
		assertFalse(claves.pertenece(9));
		assertFalse(claves.pertenece(6));
		
		ConjuntoTDA valoresClave7 = d.recuperar(7);
		assertTrue(valoresClave7.pertenece(71));
		assertTrue(valoresClave7.pertenece(73));
		assertTrue(valoresClave7.pertenece(75));
		assertFalse(valoresClave7.pertenece(77));
		
		ConjuntoTDA valoresClave2 = d.recuperar(2);
		assertTrue(valoresClave2.pertenece(21));
		assertTrue(valoresClave2.pertenece(25));
		assertFalse(valoresClave2.pertenece(26));
	}
}
