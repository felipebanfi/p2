package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmos.EjercicioUsoTdaParcialUno;
import implementaciones.ABB;
import implementaciones.ConjuntoDinamico;
import tdas.ABBTDA;
import tdas.ConjuntoTDA;

public class TestEjercicioUsoTdaParcialUno 
{
	@Test
	public void testDeterminarSiAlgunoDeLosElementosCoincideConLaCantidad()
	{
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		c.agregar(1);
		assertTrue(EjercicioUsoTdaParcialUno.determinarSiAlgunoDeLosElementosCoincideConLaCantidad(c));
		
		c.agregar(1);
		c.agregar(2);
		assertTrue(EjercicioUsoTdaParcialUno.determinarSiAlgunoDeLosElementosCoincideConLaCantidad(c));
		
		c.agregar(1);
		c.agregar(2);
		c.agregar(3);
		assertTrue(EjercicioUsoTdaParcialUno.determinarSiAlgunoDeLosElementosCoincideConLaCantidad(c));
		
		c.agregar(1);
		c.agregar(2);
		c.agregar(3);
		c.agregar(5);
		assertFalse(EjercicioUsoTdaParcialUno.determinarSiAlgunoDeLosElementosCoincideConLaCantidad(c));
	}
	
	@Test
	public void testCuantosNodosTienenUnSoloHijo()
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
		a.agregar(5);
		a.agregar(1);
		a.agregar(7);
		a.agregar(6);
		a.agregar(15);
		a.agregar(11);
		a.agregar(25);
		a.agregar(20);
		a.agregar(27);
		a.agregar(18);
		
		assertEquals(2, EjercicioUsoTdaParcialUno.cuantosNodoTienenUnSoloHijo(a));
	}
	
	@Test
	public void testCuantosNodosTienenUnSoloHijoOtraPrueba()
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
		a.agregar(5);
				
		assertEquals(1, EjercicioUsoTdaParcialUno.cuantosNodoTienenUnSoloHijo(a));
	}
	
	@Test
	public void testCuantosNodosTienenUnSoloHijoYOtraPrueba()
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
						
		assertEquals(0, EjercicioUsoTdaParcialUno.cuantosNodoTienenUnSoloHijo(a));
	}
}
