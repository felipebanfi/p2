package tests;

import static org.junit.Assert.*;
import implementaciones.PilaDinamica;

import org.junit.Test;

import algoritmos.EjerciciosPila;

import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class TestEjerciciosPila
{

	@Test
	public void testPasarUnaPilaAOtraInvertida()
	{
		PilaTDA original = new PilaDinamica();
		original.inicializar();
		
		original.apilar(2);
		original.apilar(1);
		original.apilar(5);
		original.apilar(3);
		original.apilar(6);
		
		PilaTDA auxiliar = new PilaDinamica();
		auxiliar.inicializar();
		
		auxiliar.apilar(2);
		auxiliar.apilar(1);
		auxiliar.apilar(5);
		auxiliar.apilar(3);
		auxiliar.apilar(6);
		
		PilaTDA invertida = EjerciciosPila.pasarUnaPilaAOtraInvertida(auxiliar);
		invertida = EjerciciosPila.pasarUnaPilaAOtraInvertida(invertida);
		
		while (!original.estaVacia())
		{
			assertEquals(original.tope(), invertida.tope());
			
			original.desapilar();
			invertida.desapilar();
		}
	}

	@Test
	public void testContarElementos()
	{
		PilaTDA original = new PilaDinamica();
		original.inicializar();
		
		original.apilar(2);
		original.apilar(1);
		original.apilar(5);
		original.apilar(3);
		original.apilar(6);
		
		int cantidad = EjerciciosPila.contarElementos(original);
		
		assertEquals(5,cantidad);
	}

	@Test
	public void testSumaDeElementos()
	{
		PilaTDA original = new PilaDinamica();
		original.inicializar();
		
		original.apilar(2);
		original.apilar(1);
		original.apilar(5);
		original.apilar(3);
		original.apilar(6);
		
		int suma = EjerciciosPila.sumaDeElementos(original);
		
		assertEquals(17,suma);
	}

	@Test
	public void testPromedio()
	{
		PilaTDA original = new PilaDinamica();
		original.inicializar();
		
		original.apilar(2);
		original.apilar(1);
		original.apilar(5);
		original.apilar(3);
		original.apilar(6);
		
		double promedio = EjerciciosPila.promedio(original);
		
		assertEquals(17/5, promedio,0.01);
	}

	@Test
	public void testEsCapicua()
	{
		PilaTDA pila = new PilaDinamica();
		
		pila.inicializar();
		pila.apilar(2);
		pila.apilar(1);
		pila.apilar(5);
		pila.apilar(3);
		pila.apilar(6);
		assertFalse(EjerciciosPila.esCapicua(pila));
		
		pila.inicializar();
		pila.apilar(1);
		pila.apilar(3);
		pila.apilar(3);
		pila.apilar(1);
		assertTrue(EjerciciosPila.esCapicua(pila));
		
		pila.inicializar();
		pila.apilar(15);
		pila.apilar(2);
		pila.apilar(7);
		pila.apilar(7);
		pila.apilar(2);
		pila.apilar(15);
		assertTrue(EjerciciosPila.esCapicua(pila));
		
		pila.inicializar();
		pila.apilar(8);
		pila.apilar(9);
		pila.apilar(55);
		pila.apilar(3);
		pila.apilar(55);
		pila.apilar(9);
		pila.apilar(8);
		assertTrue(EjerciciosPila.esCapicua(pila));
	}

	@Test
	public void testEliminarRepeticiones()
	{
		PilaTDA pila = new PilaDinamica();
		
		pila.inicializar();
		pila.apilar(2);
		pila.apilar(1);
		pila.apilar(5);
		pila.apilar(3);
		pila.apilar(6);
		int [] array = new int[5];
		array[0] = 6;
		array[1] = 3;
		array[2] = 5;
		array[3] = 1;
		array[4] = 2;
		int i = 0;
				
		EjerciciosPila.eliminarRepeticiones(pila);
		
		while (!pila.estaVacia())
		{
			assertEquals(array[i], pila.tope());
			pila.desapilar();
			i++;
		}
		
		pila.inicializar();
		pila.apilar(2);
		pila.apilar(1);
		pila.apilar(2);
		int [] array1 = new int[2];
		array1[0] = 1;
		array1[1] = 2;
		int j = 0;
		
		EjerciciosPila.eliminarRepeticiones(pila);
				
		while (!pila.estaVacia())
		{
			assertEquals(array1[j], pila.tope());
			pila.desapilar();
			j++;
		}
		
		pila.inicializar();
		pila.apilar(2);
		pila.apilar(1);
		pila.apilar(5);
		pila.apilar(3);
		pila.apilar(6);
		pila.apilar(5);
		pila.apilar(3);
		int [] array2 = new int[5];
		array2[0] = 6;
		array2[1] = 3;
		array2[2] = 5;
		array2[3] = 1;
		array2[4] = 2;
		int k = 0;
				
		EjerciciosPila.eliminarRepeticiones(pila);
		
		while (!pila.estaVacia())
		{
			assertEquals(array2[k], pila.tope());
			pila.desapilar();
			k++;
		}
	}

	@Test
	public void testElementosRepetidos()
	{
		PilaTDA pila = new PilaDinamica();
		
		pila.inicializar();
		pila.apilar(2);
		pila.apilar(1);
		pila.apilar(5);
		pila.apilar(3);
		pila.apilar(6);
		assertTrue(EjerciciosPila.elementosRepetidos(pila).estaVacio());
		
		pila.inicializar();
		pila.apilar(1);
		pila.apilar(3);
		pila.apilar(7);
		pila.apilar(1);
		ConjuntoTDA c = EjerciciosPila.elementosRepetidos(pila);
		assertFalse(c.estaVacio());		
		assertTrue(c.pertenece(1));
		assertFalse(c.pertenece(3));
	}

}
