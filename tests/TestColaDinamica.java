package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.ColaDinamica;
import tdas.ColaTDA;

public class TestColaDinamica 
{

	@Test
	public void testInicializarGeneraUnaColaVacia()
	{
		ColaTDA cola = new ColaDinamica();
		
		cola.inicializar();
		assertTrue(cola.estaVacia());
	
		cola.acolar(1);
		
		cola.inicializar();
		assertTrue(cola.estaVacia());
	}
	
	@Test
	public void testQueSiSoloAcoloElPrimeroEsSiempreElMismo()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		cola.acolar(1);
		assertEquals(1, cola.primero());
		cola.acolar(2);
		assertEquals(1, cola.primero());
		cola.acolar(3);
		assertEquals(1, cola.primero());
		cola.acolar(4);
		assertEquals(1, cola.primero());
	}
	
	@Test
	public void testQueSiAcoloYDesacoloLaColaQuedaVacia()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		cola.acolar(1);
		assertFalse(cola.estaVacia());
		
		cola.desacolar();
		assertTrue(cola.estaVacia());
	}
	
	
	@Test
	public void testQueSeaFIFO()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		cola.acolar(100);
		cola.acolar(26);
		cola.acolar(31);
		cola.acolar(49);
		
		assertEquals(100, cola.primero());
		cola.desacolar();
		
		assertEquals(26, cola.primero());
		cola.desacolar();
		
		assertEquals(31, cola.primero());
		cola.desacolar();
		
		assertEquals(49, cola.primero());
		cola.desacolar();
	}
	
	@Test
	public void testQueSeAcolanYDesacolanLaMismaCantidadDeElementos()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		cola.acolar(100);
		cola.acolar(26);
		cola.acolar(31);
		cola.acolar(49);
		
		int cantidadDeElementos = 0;
		
		while (!cola.estaVacia())
		{
			cantidadDeElementos++;
			cola.desacolar();
		}
		
		assertEquals(4, cantidadDeElementos);
	}

}
