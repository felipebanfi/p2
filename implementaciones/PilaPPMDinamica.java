package implementaciones;

import tdas.PilaPPMTDA;

public class PilaPPMDinamica implements PilaPPMTDA 
{
	class Nodo
	{
		int valor;
		Nodo siguiente;
	}
	
	private Nodo tope;
	
	@Override
	public void inicializar()
	{
		tope = null;
	}

	@Override
	public boolean estaVacia()
	{
		return tope == null;
	}

	@Override
	public int tope() 
	{
		return tope.valor;
	}

	@Override
	public void apilar(int elemento) 
	{
		boolean sePuedeApilar = chequearQueSePuedaApilar(elemento);
		
		if (sePuedeApilar)
		{
			Nodo nuevo = new Nodo();
			nuevo.valor = elemento;
			nuevo.siguiente = tope;
			tope = nuevo;
		}
	}

	private boolean chequearQueSePuedaApilar(int elemento)
	{
		boolean sePuedeApilar = true;
		
		Nodo aux = tope;
		
		while (sePuedeApilar && aux != null)
		{
			if (aux.valor > elemento)
			{
				sePuedeApilar = false;
			}
			
			aux = aux.siguiente;
		}
		
		return sePuedeApilar;
	}

	@Override
	public void desapilar()
	{
		tope = tope.siguiente;
	}

}
