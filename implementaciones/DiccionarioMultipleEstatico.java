package implementaciones;

import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class DiccionarioMultipleEstatico implements DiccionarioMultipleTDA {

	class Nodo
	{
		int clave;
		int [] valores;
		int cantidadValores;
	}
	
	private Nodo[] diccionario;
	private int cantidadClaves;
	
	@Override
	public void inicializar() 
	{
		diccionario = new Nodo[100];
		cantidadClaves = 0;
	}

	@Override
	public void agregar(int clave, int valor)
	{
		int posicionDeLaClave = claveToIndice(clave);
		
		if (posicionDeLaClave == -1)
		{
			posicionDeLaClave = cantidadClaves;
			diccionario[posicionDeLaClave] = new Nodo();
			diccionario[posicionDeLaClave].clave = clave;
			diccionario[posicionDeLaClave].cantidadValores = 0;
			diccionario[posicionDeLaClave].valores = new int[100];
			cantidadClaves++;
		}
		
		Nodo elemento = diccionario[posicionDeLaClave];
		int posisiconValor = valorToIndice(elemento,valor);
		if (posisiconValor == -1 )
		{
			elemento.valores[elemento.cantidadValores] = valor;
			elemento.cantidadValores++;
		}
	}

	@Override
	public void eliminar(int clave)
	{
		int posicionDeLaClave = claveToIndice(clave);
		if (posicionDeLaClave != -1)
		{
			diccionario[posicionDeLaClave] = diccionario[cantidadClaves -1];
			cantidadClaves--;
		}

	}

	@Override
	public void eliminarValor(int clave, int valor)
	{
		int posicionDeLaClave = claveToIndice(clave);
		if (posicionDeLaClave != -1)
		{
			Nodo elemento = diccionario[posicionDeLaClave];
			
			int posicionValor = 0;
			posicionValor = valorToIndice(elemento,valor);
			
			if (posicionValor != -1)
			{
				elemento.valores[posicionValor] = elemento.valores[elemento.cantidadValores - 1];
				elemento.cantidadValores--;
				
				if (elemento.cantidadValores == 0)
				{
					eliminar(clave);
				}
			}
		}

	}

	@Override
	public ConjuntoTDA claves()
	{
		ConjuntoTDA claves = new ConjuntoEstatico();
		claves.inicializar();
		
		for (int i = 0; i < cantidadClaves; i++)
		{
			claves.agregar(diccionario[i].clave);
		}
		
		return claves;
	}

	@Override
	public ConjuntoTDA recuperar(int clave)
	{
		ConjuntoTDA valores = new ConjuntoEstatico();
		valores.inicializar();
		
		int posicionDeLaClave = claveToIndice(clave);
		if (posicionDeLaClave != -1 )
		{
			Nodo elemento = diccionario[posicionDeLaClave];
			
			for (int i = 0; i < elemento.cantidadValores; i++)
			{
				valores.agregar(elemento.valores[i]);
			}
		}
		
		return valores;
	}
	
	private int claveToIndice(int clave)
	{
		int indice = cantidadClaves -1;
		boolean encontrada = false;
		
		while (!encontrada && indice >= 0)
		{
			if (clave == diccionario[indice].clave)
			{
				encontrada = true;
			}
			else
			{
				indice--;
			}
		}
		
		return indice;
	}
	
	private int valorToIndice(Nodo elemento, int valor)
	{
		int posicionValor = elemento.cantidadValores -1;
		boolean encontrado = false;
		
		while (!encontrado && posicionValor >= 0)
		{
			if (valor == elemento.valores[posicionValor])
			{
				encontrado = true;
			}
			else
			{
				posicionValor--;
			}
		}
				
		return posicionValor;
	}
}
