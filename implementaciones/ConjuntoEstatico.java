package implementaciones;

import java.util.Random;

import tdas.ConjuntoTDA;

public class ConjuntoEstatico implements ConjuntoTDA {

	private int cantidad;
	private int[] conjunto;
	
	
	@Override
	public void inicializar()
	{
		cantidad = 0;
		conjunto = new int[100];
	}

	@Override
	public void agregar(int elemento)
	{
		if (!pertenece(elemento))
		{
			conjunto[cantidad] = elemento;
			cantidad++;
		}
	}

	@Override
	public void sacar(int elemento)
	{
		boolean sacado = false;
		int iterador = 0;
		
		while (!sacado && iterador < cantidad)
		{
			if (elemento == conjunto[iterador])
			{
				sacado = true;
				conjunto[iterador] = conjunto[cantidad -1];
				cantidad--;
			}
			
			iterador++;
		}
	}

	@Override
	public int elegir()
	{
		Random random = new Random();
		return conjunto[random.nextInt(cantidad)];
	}

	@Override
	public boolean estaVacio() 
	{
		return cantidad == 0;
	}

	@Override
	public boolean pertenece(int elemento) 
	{
		boolean esta = false;
		int iterador = 0;
		
		while (!esta && iterador < cantidad)
		{
			if (elemento == conjunto[iterador])
			{
				esta = true;
			}
			
			iterador++;
		}
		
		return esta;
	}

}
