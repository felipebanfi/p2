package implementaciones;

import tdas.ColaTDA;

public class ColaEstatica implements ColaTDA
{
	private int indice;
	private int [] cola;
	
	@Override
	public void inicializar() 
	{
		indice = 0;
		cola = new int [100];
	}

	@Override
	public void acolar(int elemento)
	{
		cola[indice] = elemento;
		indice++;
	}

	@Override
	public void desacolar() 
	{
		for (int i = 0; i < indice-1; ++i)
		{
			cola[i] = cola[i+1];
		}
		
		indice--;
	}

	@Override
	public int primero()
	{
		return cola[0];
	}

	@Override
	public boolean estaVacia() 
	{
		return indice == 0;
	}

}
