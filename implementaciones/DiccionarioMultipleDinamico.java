package implementaciones;

import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class DiccionarioMultipleDinamico implements DiccionarioMultipleTDA {

	class NodoValor
	{
		int valor;
		NodoValor siguiente;
	}
	
	class Nodo
	{
		int clave;
		NodoValor primerValor;
		Nodo siguiente;
	}
	
	private Nodo primero;
	
	@Override
	public void inicializar()
	{
		primero = null;
	}

	@Override
	public void agregar(int clave, int valor)
	{
		Nodo nodo = clave2Nodo(clave);
		
		if (nodo == null)
		{
			nodo = new Nodo();
			nodo.clave = clave;
			nodo.siguiente = primero;
			primero = nodo;
		}
		
		NodoValor nodoValor = buscar(valor,nodo.primerValor);
		
		if (nodoValor == null)
		{
			nodoValor = new NodoValor();
			nodoValor.valor = valor;
			nodoValor.siguiente = nodo.primerValor;
			nodo.primerValor = nodoValor;
		}
	}

	private NodoValor buscar(int valor, NodoValor primerValor)
	{
		NodoValor nodo = primerValor;
		
		while (nodo != null && nodo.valor != valor)
		{
			nodo = nodo.siguiente;
		}
		
		return nodo;
	}

	private Nodo clave2Nodo(int clave)
	{
		Nodo aux = primero;
		
		while (aux != null && aux.clave != clave)
		{
			aux = aux.siguiente;
		}
		
		return aux;
	}

	@Override
	public ConjuntoTDA claves()
	{
		ConjuntoTDA claves = new ConjuntoDinamico();
		claves.inicializar();
		
		Nodo iterador = primero;
		
		while (iterador != null)
		{
			claves.agregar(iterador.clave);
			iterador = iterador.siguiente;
		}
		
		return claves;
	}

	@Override
	public ConjuntoTDA recuperar(int clave)
	{
		ConjuntoTDA valores = new ConjuntoDinamico();
		valores.inicializar();
		
		Nodo buscado = clave2Nodo(clave);
		
		if (buscado != null)
		{
			NodoValor nv = buscado.primerValor;
			
			while (nv != null)
			{
				valores.agregar(nv.valor);
				nv = nv.siguiente;
			}
		}
		
		return valores;
	}

	@Override
	public void eliminar(int clave)
	{
		if (primero != null)
		{
			if (primero.clave == clave)
			{
				primero = primero.siguiente;
			}
			else
			{
				Nodo anterior = primero;
				Nodo actual = primero.siguiente;
				
				while (actual != null &&
					   actual.clave != clave)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual != null)
				{
					anterior.siguiente = actual.siguiente;
				}
			}
		}
	}
	

	@Override
	public void eliminarValor(int clave, int valor)
	{
		Nodo buscado = clave2Nodo(clave);
		
		if (buscado != null)
		{
			if (buscado.primerValor.valor == valor)
			{
				buscado.primerValor = buscado.primerValor.siguiente;
				
				if (buscado.primerValor == null)
				{
					eliminar(clave);
				}
			}		
			else
			{
				NodoValor anterior = buscado.primerValor;
				NodoValor actual = buscado.primerValor.siguiente;
				
				while (actual != null && actual.valor != valor)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual != null)
				{
					anterior.siguiente = actual.siguiente;
				}
			}
		}
	}

}
