package implementaciones;

import java.util.Random;

import tdas.ConjuntoTDA;

public class ConjuntoDinamico implements ConjuntoTDA {

	class Nodo
	{
		int valor;
		Nodo siguiente;
	}
	
	private Nodo primero;
	private int cantidad;
	
	@Override
	public void inicializar() 
	{
		primero = null;
		cantidad = 0;

	}

	@Override
	public void agregar(int elemento)
	{
		if (!pertenece(elemento))
		{
			Nodo nuevo = new Nodo();
			nuevo.valor = elemento;
			nuevo.siguiente = primero;
			primero = nuevo;
			cantidad++;
		}

	}

	@Override
	public void sacar(int elemento) 
	{
		if (primero != null)
		{
			if (primero.valor == elemento)
			{
				primero = primero.siguiente;
			}
			else
			{
				Nodo anterior = primero;
				Nodo actual = primero.siguiente;
				
				while (actual.valor != elemento &&
					   actual.siguiente != null)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual.valor == elemento)
				{
					anterior.siguiente = actual.siguiente;
				}
				
			}
			
			cantidad--;
		}
	}

	@Override
	public int elegir() 
	{
		Nodo recorro = primero;
		Random r = new Random();
		
		int x = r.nextInt(cantidad);
		
		for (int i = 0; i < x ; i++)
		{
			recorro = recorro.siguiente;
		}
		
		return recorro.valor;
	}

	@Override
	public boolean estaVacio()
	{
		return primero == null;
	}

	@Override
	public boolean pertenece(int elemento)
	{
		boolean pertenece = false;
		Nodo iterador = primero;
		
		while (!pertenece && iterador != null)
		{
			if (iterador.valor == elemento)
			{
				pertenece = true;
			}
			
			iterador = iterador.siguiente;
		}
		
		return pertenece;
	}

}
