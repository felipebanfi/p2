package implementaciones;

import tdas.ABBTDA;

public class ABB implements ABBTDA 
{
	class Nodo
	{
		int valor;
		ABBTDA hi;
		ABBTDA hd;
	}
	
	private Nodo raiz;
	
	@Override
	public void inicializar()
	{
		raiz = null;
	}

	@Override
	public int raiz()
	{
		return raiz.valor;
	}

	@Override
	public boolean estaVacio()
	{
		return raiz == null;
	}

	@Override
	public ABBTDA hijoIzquierdo()
	{
		return raiz.hi;
	}

	@Override
	public ABBTDA hijoDerecho()
	{
		return raiz.hd;
	}

	@Override
	public void agregar(int elemento) 
	{	
		if (raiz == null)
		{
			raiz = new Nodo();
			raiz.valor = elemento;
			
			raiz.hi = new ABB();
			raiz.hi.inicializar();
			
			raiz.hd = new ABB();
			raiz.hd.inicializar();
		}
		else
		{
			if (elemento < raiz.valor)
			{
				raiz.hi.agregar(elemento);
			}
			else
			{
				if (elemento > raiz.valor)
				{
					raiz.hd.agregar(elemento);
				}
			}
		}
		
	}

	@Override
	public void eliminar(int elemento)
	{
		if (raiz != null)
		{
			if (raiz.valor == elemento &&
				soyUnaHoja())
			{
				raiz = null;
			}
			else
			{
				if (raiz.valor == elemento &&
					raiz.hi.estaVacio() && 
					!raiz.hd.estaVacio())
				{
					int menor = menor(raiz.hd);
					raiz.valor = menor;
					raiz.hd.eliminar(menor);
				}
				else
				{
					if (raiz.valor == elemento &&
						!raiz.hi.estaVacio())
					{
						int mayor = mayor(raiz.hi);
						raiz.valor = mayor;
						raiz.hi.eliminar(mayor);
					}
					else
					{
						if (raiz.valor > elemento)
						{
							raiz.hi.eliminar(elemento);
						}
						else
						{
							raiz.hd.eliminar(elemento);
						}
					}
				}
			}
		}	
	}

	private int mayor(ABBTDA arbol) 
	{
		if (arbol.hijoDerecho().estaVacio())
		{
			return arbol.raiz();
		}
		else
		{
			return mayor(arbol.hijoDerecho());
		}
	}

	private int menor(ABBTDA arbol)
	{
		if (arbol.hijoIzquierdo().estaVacio())
		{
			return arbol.raiz();
		}
		else
		{
			return menor(arbol.hijoIzquierdo());
		}
	}

	private boolean soyUnaHoja()
	{
		return raiz.hd.estaVacio() && raiz.hi.estaVacio();
	}

}
