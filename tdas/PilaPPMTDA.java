package tdas;

public interface PilaPPMTDA
{
	/**
	 * inicializa la estructura
	 * PRE:
	 * POS:
	 */
	public void inicializar();
	
	/**
	 * indica si la pila esta vacia
	 * PRE: inicializada
	 * POS:
	 */
	public boolean estaVacia();
	
	/**
	 * devuelve el valor del ultimo elemento ingresado
	 * PRE: inicializada y no vacia
	 * POS:
	 */
	public int tope();
	
	/**
	 * apila en elemento si su valor no es menor a los que ya estan
	 * PRE: inicializada
	 * POS: si se apila, la estructura queda con un elemento mas
	 */
	public void apilar(int elemento);
	
	/**
	 * desapila el ultimo elemento ingresado
	 * PRE: inicializado y no vacia
	 * POS: la estructura tiene un elemento menos
	 */
	public void desapilar();
}
